import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-on-boarding',
  templateUrl: './on-boarding.component.html',
  styleUrls: ['./on-boarding.component.css']
})
export class OnBoardingComponent implements OnInit {
  public email: string;
  public firstname: string;
  public lastname: string;
  public businessname: string;
  public locationbusiness: string;
  public companywebsite: string;
  public tellaboutbusiness: string;
  public additionaldetail: string;
  public inlineRadioOptions: string;
  public phonenumber: number;
  public errors = false;


  constructor(private client: HttpClient) { }

  ngOnInit() {
  }

  onSubmit() {
    // tslint:disable-next-line:max-line-length
    if (!this.email || !this.firstname || !this.lastname || !this.phonenumber || !this.businessname ||
      !this.locationbusiness || !this.companywebsite || !this.tellaboutbusiness || !this.inlineRadioOptions) {
        this.errors = true;
    } else {
      this.errors = false;
      this.client.post<any>('https://httpbin.org/post', {
        email: this.email,
        firstname: this.firstname,
        lastname: this.lastname,
        phonenumber: this.phonenumber,
        businesname: this.businessname,
        locationbusiness: this.locationbusiness,
        companywebsite: this.companywebsite,
        tellaboutbusiness: this.tellaboutbusiness,
        probability: this.inlineRadioOptions
      }).subscribe(resp => {
        console.log(resp);
      });
    }
  }
}
