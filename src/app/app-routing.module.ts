import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnBoardingComponent } from './on-boarding/on-boarding.component';


const routes: Routes = [
  {
    path:  'form', component: OnBoardingComponent
  },
  { path: '',
    redirectTo: '/form',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
